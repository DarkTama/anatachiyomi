# Anatachiyomi

is a simple app that grabs MyAnimeList anime data using Jikan API and then views it.


## Tech used

 - ReactNative(Expo)
 - VSCode
 - Jikan API
 - NPM
 - Figma
 - Realme 3 Android 10 (during the development)

## API

API used https://jikan.moe

## References

 - https://blog.sanbercode.com/ 
 - https://stackoverflow.com/
 - https://www.youtube.com/
 - https://docs.expo.io/
 - https://reactnavigation.org/
 - https://www.npmjs.com/
 - https://reactnative.dev/
 - etc

## Demo

Video demo: https://youtu.be/vY0pccNQA4o

## Link

 - APK: https://drive.google.com/file/d/1Sn71994It2l2Sn16VU3sG8OEnNSYZkGo/view?usp=sharing
 - Figma Mockup: https://www.figma.com/file/YI8FQqyXgHTkyRgqyal8Ux/Final-Project---Anatachiyomi?node-id=0%3A1

## App Login

 - Nickname: Your Nickname
 - Password: 12341234