import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";
import { View, Text } from "react-native";
import React, { useContext } from "react";
import Home from "../Screens/Home";
import Season from "../Screens/Search";
import Details from "../Screens/Details";
import Login from "../Screens/Login";
import About from "../Screens/About";
import { AuthContext } from "../Contexts/AuthContext";
import { AntDesign } from '@expo/vector-icons';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

const AppDrawer = (props) => {
  const { signOut, authState } = useContext(AuthContext);
  return (
    <DrawerContentScrollView {...props} contentContainerStyle={{ flex: 1 }}>
      <View style={{marginVertical:20, alignItems:"flex-end", marginHorizontal:15}}>
        <Text>Hi, {authState.username}!</Text>
      </View>
      <DrawerItemList {...props} />
      <View style={{ flex: 1, marginVertical: 20 }}>
        <DrawerItem
          label="Log out"
          onPress={signOut}
          style={{ flex: 1, justifyContent: "flex-end" }}
        />
      </View>
    </DrawerContentScrollView>
  );
};

const DrawerNav = () => {
  return (
    <Drawer.Navigator drawerContent={(props) => <AppDrawer {...props} />}>
      <Drawer.Screen name="Home" component={BottomTab} />
      <Drawer.Screen name="About" component={About} />
    </Drawer.Navigator>
  );
};

const BottomTab = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Anime"
        component={Home}
        options={{ 
          tabBarLabel:'',
          headerShown: false,
          tabBarIcon:({color}) =>(
            <AntDesign name="book" size={22} color={color} />
          )
        }}
      />
      <Tab.Screen
        name="Search"
        component={Season}
        options={{ 
          headerShown: false,
          tabBarLabel:'',
          tabBarIcon:({color})=>(
            <AntDesign name="search1" size={22} color={color} />
          )
         }}
      />
    </Tab.Navigator>
  );
};

const Navigation = () => {
  const { authState } = useContext(AuthContext);
  const isLoggedIn = authState.isSignedIn;
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {isLoggedIn ? (
          <>
            <Stack.Group>
              <Stack.Screen
                name="Anime"
                component={DrawerNav}
                options={{ headerShown: false }}
              />
            </Stack.Group>
            <Stack.Group screenOptions={{ presentation: "modal" }}>
              <Stack.Screen
                name="Anime Details"
                component={Details}
                // options={{ headerShown: false }}
              />
            </Stack.Group>
          </>
        ) : (
          <Stack.Screen name="Login" component={Login} options={{headerShown:false}} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
