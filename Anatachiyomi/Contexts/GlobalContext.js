import * as React from "react";
import axios from "axios";

export const GlobalContext = React.createContext();

export const GlobalProvider = ({ children, navigation }) => {
  const apiGlobal = "https://api.jikan.moe/v4/anime";
  const apiTop = "https://api.jikan.moe/v4/top/anime";
  const apiSeason = "https://api.jikan.moe/v4/seasons/now";
  const [data, setData] = React.useState();
  const [dataSeason, setDataSeason] = React.useState();
  const [anime, setAnime] = React.useState();
  const [isLoading, setIsLoading] = React.useState(true);
  const [inputAnime, setInputAnime] = React.useState("");

  const getData = () => {
    axios
      .get(`${apiTop}?sfw=true&limit=20`)
      .then((res) => {
        return setData(res.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getSeason = () => {
    axios
      .get(`${apiSeason}?sfw=true&limit=20`)
      .then((res) => {
        return setDataSeason(res.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAnime = (query) => {
    axios
      .get(`${apiGlobal}?sfw=true&q=${query}&limit=20`)
      .then((res) => {
        setAnime(res.data.data);
        return setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const SearchAnime = (query) => {
    return getAnime(query);
    // return setIsLoading(true);
  };

  const handleRefresh = () => {
    if (isLoading === false) {
      setIsLoading(true);
    }
  };

  React.useEffect(() => {
    if (isLoading === true) {
      getAnime();
      getData();
      getSeason();
      return setIsLoading(false);
    }
  }, [isLoading, setIsLoading]);

  return (
    <GlobalContext.Provider
      value={{
        data,
        dataSeason,
        setIsLoading,
        handleRefresh,
        anime,
        SearchAnime,
        inputAnime,
        setInputAnime,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
