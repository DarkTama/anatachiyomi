import React, { createContext, useState } from "react";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [error, setError] = useState("");
  const pwd = "12341234"
  const [auth, setAuth] = useState({
    isSignedIn: false,
    username:""
  });

  const signIn = ({ username, password }) => {
    // if (username !== "" && password === pwd) {
    //   setAuth({
    //     isSignedIn: true,
    //     username: username
    //   });
    // } 
    if (username !== "") {
      setAuth({
        isSignedIn: true,
        username: username
      });
    } 
    else {
      setError("Harap masukkan Nickname!");
      setTimeout(() => {
        setError("");
      }, 3000);
    }
  };

  const signOut = () => {
    setAuth({
      isSignedIn: false,
      username: "",
    });
  };

  return (
    <AuthContext.Provider
      value={{
        signIn,
        signOut,
        authState: auth,
        error,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
