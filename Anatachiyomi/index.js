import React from "react";
import { AuthProvider } from "./Contexts/AuthContext";
import { GlobalContext, GlobalProvider } from "./Contexts/GlobalContext";
import Navigation from "./Navigations/Navigation";
import Home from "./Screens/Home";

const index = () => {
  return (
    <AuthProvider>
      <GlobalProvider>
        <Navigation />
      </GlobalProvider>
    </AuthProvider>
  );
};

export default index;
