import * as React from "react";
import { 
    View, 
    Text,
    TextInput,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import { AuthContext } from "../Contexts/AuthContext";

const LoginScreen = () =>{
    const {signIn, authState, error} = React.useContext(AuthContext);
    const [username, setUsername] = React.useState("");
    const [password, setPassword] = React.useState("");

    return(
        <>
        <View style={styles.container}>
            <View style={styles.logoView}>
                <View style={{flexWrap:"wrap", flexDirection:"row"}}>
                    <Text style={styles.logoAna}>Ana</Text>
                    <Text style={styles.logoTachiyomi}>tachiyomi</Text>
                </View>
                <View style={{alignItems:"center"}}>
                    <Image 
                        style={styles.logoImage}
                        source={require('../Assets/logo.png')}
                    />
                </View>
            </View>

            <View>
                <Text style={styles.logoLogin}>Login</Text>
            </View>
            
            <View style={styles.inputView}>
                <TextInput
                    style={styles.textInput}
                    placeholder="Nickname"
                    placeholderTextColor="#b3b3b3"
                    onChangeText={setUsername}
                    value={username}
                />
            </View>
            {/* <View style={styles.inputView2}>
                <TextInput
                    style={styles.textInput}
                    placeholder="Password"
                    editable={false}
                    selectTextOnFocus={false}
                    placeholderTextColor="#b3b3b3"
                    secureTextEntry={true}
                    onChangeText={setPassword}
                    value={password}
                />
            </View> */}

            <Text style={styles.errorView}>{error}</Text>

            <TouchableOpacity style={styles.loginButton} onPress={()=>signIn({username, password})}>
                <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
        </View>
        </>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6e6e6',
        alignItems: 'center',
        justifyContent: 'center',
    },
    errorView:{
        color:'red',
        fontWeight:"bold"
    },
    logoAna:{
        fontSize:36,
        color:'#FF8C8C'
    },
    logoTachiyomi:{
        fontSize:36,
    },
    logoView:{
        flexDirection:"column",
        flexWrap:"wrap",
        marginBottom:100
    },
    logoImage:{
        width:80,
        height:80,
        resizeMode:"contain"
    },
    logoLogin:{
        fontWeight:"bold",
        fontSize:28,
        marginBottom:30,
    },
    inputView: {
        backgroundColor: "#fff",
        borderRadius: 5,
        width: "90%",
        height: 45,
        marginBottom: 10,
        alignItems: "flex-start",
    },
    inputView2: {
        backgroundColor: "#cccccc",
        borderRadius: 5,
        width: "90%",
        height: 45,
        marginBottom: 10,
        alignItems: "flex-start",
    },
    textInput: {
        height: 50,
        flex: 1,
        width:'100%',
        marginLeft: 20,
    },
    loginButton:{
        width:"90%",
        borderRadius:5,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:120,
        backgroundColor:"#1F86FF",
    },
    loginText:{
        fontWeight:"bold",
        color:'#fff'
    },
    registerButton:{
        width:"90%",
        borderRadius:5,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:10,
        backgroundColor:"#e6e6e6",
        borderWidth:1,
        borderColor: "#999999"
    },
    registerText:{
        fontWeight:"bold",
        color:'#1F86FF'
    }
})

export default LoginScreen