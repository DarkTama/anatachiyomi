import React, { useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ActivityIndicator,
} from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { GlobalContext } from "../Contexts/GlobalContext";

const Home = ({ navigation }) => {
  const { data, dataSeason } = useContext(GlobalContext);
  const title = (tit) => {
    if (tit.title_english !== null) {
      return tit.title_english;
    } else {
      return tit.title;
    }
  };

  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        <View>
          <Text
            style={{
              marginTop: 10,
              marginLeft: 5,
              fontWeight: "bold",
              fontSize: 20,
            }}
          >
            Top Anime:
          </Text>
          {!data ? (
            <ActivityIndicator />
          ) : (
            <FlatList
              horizontal
              style={styles.flatView}
              data={data}
              keyExtractor={(item) => item.mal_id}
              renderItem={({ item }) => (
                <View style={styles.itemView1}>
                  <TouchableOpacity
                    style={styles.imageWrapper1}
                    onPress={() =>
                      navigation.navigate("Anime Details", {
                        array: item,
                      })
                    }
                  >
                    <ImageBackground
                      style={styles.image}
                      source={{
                        uri: `${item.images.jpg.image_url}`,
                      }}
                    >
                      <View style={styles.imageDetail}>
                        <Text numberOfLines={2} style={styles.textDetail}>
                          {title(item)}
                        </Text>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                </View>
              )}
            />
          )}
        </View>
        <View style={{ flex: 1 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, marginLeft: 5 }}>
            Current Season Anime:
          </Text>
          {!dataSeason ? (
            <ActivityIndicator />
          ) : (
            <FlatList
              style={styles.flatView}
              data={dataSeason}
              keyExtractor={(item) => item.mal_id}
              numColumns={2}
              columnWrapperStyle={styles.flatRow}
              renderItem={({ item }) => (
                <View style={styles.itemView}>
                  <TouchableOpacity
                    style={styles.imageWrapper}
                    onPress={() =>
                      navigation.navigate("Anime Details", {
                        array: item,
                      })
                    }
                  >
                    <ImageBackground
                      style={styles.image}
                      source={{
                        uri: `${item.images.jpg.image_url}`,
                      }}
                    >
                      <View style={styles.imageDetail}>
                        <Text numberOfLines={2} style={styles.textDetail}>
                          {title(item)}
                        </Text>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                </View>
              )}
            />
          )}
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e6e6e6",
  },
  flatView: {
    width: "100%",
  },
  flatRow: {
    flex: 1,
    justifyContent: "space-around",
  },
  itemView: {
    margin: 5,
    padding: 5,
    flexDirection: "column",
    flexWrap: "wrap",
  },
  itemView1: {
    padding: 5,
    flexDirection: "column",
    flexWrap: "wrap",
  },
  imageWrapper: {
    height: 300,
    width: 200,
    overflow: "hidden",
    borderRadius: 5,
  },
  imageWrapper1: {
    height: 200,
    width: 160,
    overflow: "hidden",
    borderRadius: 5,
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    justifyContent: "flex-end",
  },
  imageDetail: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 5,
    height: 37,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.6)",
  },
  textDetail: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
});
export default Home;
