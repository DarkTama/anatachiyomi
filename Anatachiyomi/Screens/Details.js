import { View, Text, Image, StyleSheet, ScrollView } from "react-native";
import YoutubePlayer from "react-native-youtube-iframe";

const Details = ({ route, navigation }) => {
  const { array } = route.params;

  const air = (status) => {
    if (status === "Not yet aired") {
      return <Text style={{ paddingBottom: 5 }}>{array.status}</Text>;
    } else {
      return (
        <>
          <Text style={{ paddingBottom: 5 }}>{array.status}</Text>
          <Text style={{ fontWeight: "bold" }}>Score:</Text>
          <Text style={{ paddingBottom: 5 }}>
            {array.score}, scored by: {array.scored_by} users
          </Text>
        </>
      );
    }
  };

  const episodes = (epi) => {
    if (epi !== null) {
      return <Text>{array.episodes}</Text>;
    } else {
      return <Text>Not yet aired</Text>;
    }
  };

  const title = (tit) => {
    if (tit !== null) {
      return array.title_english;
    } else {
      return array.title;
    }
  };

  const rating = (rate) => {
    if (rate !== null) {
      return array.rating;
    } else {
      return "Not yet rated";
    }
  };

  const video = (embed) => {
    if (embed !== null) {
      return (
        <YoutubePlayer
          webViewStyle={{ opacity: 0.99 }}
          width={400}
          height={240}
          play={true}
          videoId={array.trailer.youtube_id}
        />
      );
    } else {
      return image(array.trailer.images.medium_image_url);
    }
  };

  const image = (link) => {
    if (link !== null) {
      return (
        <View style={styles.imageView}>
          <Image
            style={styles.image}
            source={{
              uri: `${array.trailer.images.medium_image_url}`,
            }}
          />
        </View>
      );
    } else {
      return (
        <Image
          style={styles.image}
          source={require("../Assets/No_image_available.png")}
        />
      );
    }
  };

  const typeAnime = (type) => {
    if (type === "Movie") {
      return (
        <>
          <Text>Movie</Text>
          <Text>{array.duration}</Text>
        </>
      );
    } else {
      return (
        <>
          <Text>TV Series</Text>
          <Text>Episodes: {episodes(array.episodes)}</Text>
        </>
      );
    }
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.animeView}>
        <YoutubePlayer
          webViewStyle={{ opacity: 0.99 }}
          width={400}
          height={240}
          play={true}
          videoId={array.trailer.youtube_id}
        />
        <View style={styles.textView}>
          <Text style={{ fontWeight: "bold", fontSize: 20 }}>
            {title(array.title_english)}
          </Text>
          {typeAnime(array.type)}
          <Text style={{ fontWeight: "bold", paddingTop: 5 }}>Synopsis:</Text>
          <Text style={{ textAlign: "justify", paddingBottom: 5 }}>
            {array.synopsis}
          </Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
            {array.genres.map((item) => {
              return (
                <View
                  style={{
                    backgroundColor: "rgba(0, 0, 0, 0.1)",
                    padding: 5,
                    margin: 2,
                    borderRadius: 6,
                  }}
                >
                  <Text>{item.name}</Text>
                </View>
              );
            })}
          </View>
          <Text style={{ fontWeight: "bold" }}>Rating:</Text>
          <Text style={{ paddingBottom: 5 }}>{rating(array.rating)}</Text>
          <Text style={{ fontWeight: "bold" }}>Status:</Text>
          {air(array.status)}
          <Text style={{ fontWeight: "bold", paddingTop: 5 }}>Studio:</Text>
          {array.studios.map((item) => {
            return <Text>- {item.name}</Text>;
          })}
          <Text style={{ fontWeight: "bold", paddingTop: 5 }}>Producer:</Text>
          {array.producers.map((item) => {
            return <Text>- {item.name}</Text>;
          })}
          <Text style={{ fontWeight: "bold" }}>Aired:</Text>
          <Text>{array.aired.string}</Text>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginBottom: 10,
    // marginTop: 20,
    backgroundColor:'#e6e6e6'
  },
  image: {
    height: 105,
    width: 380,
    margin: "2%",
    marginTop: "6%",
  },
  imageView: {
    alignItems: "center",
  },
  animeView: {
    alignSelf: "center",
  },
  textView: {
    width: "90%",
    marginLeft: 10,
    marginRight: 10,
  },
});

export default Details;
