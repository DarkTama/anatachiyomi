import React from "react";
import { View, Text, Image, StyleSheet, ScrollView } from "react-native";

import { AntDesign } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { Fontisto } from "@expo/vector-icons";

const AboutScreen = () => {
  return (
    <>
      <ScrollView
        style={{
          backgroundColor: "#e6e6e6",
        }}
      >
        <View style={styles.container}>
          <Text style={styles.aboutMeText}>About Me</Text>
          <View style={styles.profileView}>
            <View>
              <Image
                style={styles.aboutMeImage}
                source={require("../Assets/profile.png")}
              />
            </View>
            <View style={styles.profileTextView}>
              <Text style={styles.profileName}>Ekatama Ilham Prayoga</Text>
              <Text style={styles.profileTitle}>React Engineer</Text>
            </View>
          </View>

          <View style={styles.pseudoContainer}>
            <Text style={styles.aboutText}>Portofolio</Text>
            <View style={styles.iconText}>
              <AntDesign name="gitlab" size={30} color="black" />
              <Text style={styles.aboutText}> @DarkTama</Text>
            </View>
          </View>

          <View style={styles.pseudoContainer}>
            <Text style={styles.aboutText}>Media Social</Text>
            <View>
              <View style={styles.iconText}>
                <FontAwesome5 name="facebook" size={30} color="black" />
                <Text style={styles.aboutText}> Ekatama Ilham Prayoga</Text>
              </View>
              <View style={styles.iconText}>
                <FontAwesome5 name="instagram-square" size={30} color="black" />
                <Text style={styles.aboutText}> @DarkTama</Text>
              </View>
              <View style={styles.iconText}>
                <FontAwesome5 name="linkedin" size={30} color="black" />
                <Text style={styles.aboutText}> Ekatama Ilham Prayoga</Text>
              </View>
              <View style={styles.iconText}>
                <FontAwesome5 name="telegram-plane" size={30} color="black" />
                <Text style={styles.aboutText}> @ekatama1406</Text>
              </View>
            </View>
          </View>
          {/* 
          <View style={styles.pseudoContainer1}>
            <Text>Skills</Text>
            <View style={styles.iconText}>
              <Ionicons name="logo-javascript" size={24} color="black" />
              <View style={styles.skillText}>
                <Text> Javascript</Text>
                <Text>Basic</Text>
                <Text>Lang</Text>
                <Text>80%</Text>
              </View>
            </View>
            <View style={styles.iconText}>
              <FontAwesome5 name="react" size={24} color="black" />
              <View style={styles.skillText}>
                <Text> React Native</Text>
                <Text>Basic</Text>
                <Text>Lang</Text>
                <Text>60%</Text>
              </View>
            </View>
            <View style={styles.iconText}>
              <FontAwesome5 name="react" size={24} color="black" />
              <View style={styles.skillText}>
                <Text> React JS</Text>
                <Text>Basic</Text>
                <Text>Lang</Text>
                <Text>80%</Text>
              </View>
            </View>
            <Text>Tech Used</Text>
            <View style={styles.iconText}>
              <Fontisto name="visual-studio" size={24} color="black" />
              <Text> VSCode</Text>
            </View>
          </View> */}
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  pseudoContainer: {
    margin: 15,
    width: "90%",
    backgroundColor: "#fff",
    borderWidth: 1,
    alignItems: "center",
    borderColor: "#b3b3b3",
    borderRadius: 5,
    padding: 15,
  },
  pseudoContainer1: {
    margin: 15,
    width: "90%",
    backgroundColor: "#fff",
    borderWidth: 1,
    alignItems: "flex-start",
    borderColor: "#b3b3b3",
    borderRadius: 5,
    padding: 15,
  },
  aboutMeText: {
    // marginTop:35,
    margin: 15,
    fontSize: 28,
    fontWeight: "bold",
  },
  aboutMeImage: {
    width: 100,
    height: 100,
  },
  profileView: {
    flexWrap: "wrap",
    flexDirection: "row",
  },
  profileTextView: {
    marginLeft: 10,
    justifyContent: "center",
  },
  profileName: {
    fontSize: 20,
    fontWeight: "bold",
  },
  profileTitle: {
    fontSize: 18,
    color: "#999999",
  },
  iconText: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    padding: 5,
  },
  aboutText: {
    fontSize: 24,
  },
  skillText: {
    flex: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
});

export default AboutScreen;
