import React, { useContext } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  Button,
  ActivityIndicator,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { GlobalContext } from "../Contexts/GlobalContext";

const Season = ({ navigation }) => {
  const { anime, SearchAnime, inputAnime, setInputAnime, isLoading } =
    useContext(GlobalContext);
  const title = (tit) => {
    if (tit.title_english !== null) {
      return tit.title_english;
    } else {
      return tit.title;
    }
  };

  return (
    <View style={styles.container}>
      <View style={{ alignItems: "center", flex:1 }}>
        <View
          style={{
            width: "100%",
            alignItems: "center",
            justifyContent: "center",
            flexWrap: "wrap",
            flexDirection: "row",
          }}
        >
          <View
            style={{
              backgroundColor: "#fff",
              borderRadius: 5,
              width: "80%",
              height: 45,
              marginTop: 15,
              marginBottom: 5,
              marginRight: 5,
              alignItems: "flex-start",
            }}
          >
            <TextInput
              style={{
                height: 50,
                flex: 1,
                width: "100%",
                marginLeft: 20,
              }}
              placeholder="Find Anime. . ."
              onChangeText={setInputAnime}
              value={inputAnime}
            />
          </View>
          <Button title="Find" onPress={() => SearchAnime(inputAnime)} />
        </View>
        <View>
        {!anime ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            style={styles.flatView}
            data={anime}
            keyExtractor={(item) => item.mal_id}
            numColumns={2}
            columnWrapperStyle={styles.flatRow}
            renderItem={({ item }) => (
              <View style={styles.itemView}>
                <TouchableOpacity
                  style={styles.imageWrapper}
                  onPress={() =>
                    navigation.navigate("Anime Details", {
                      array: item,
                    })
                  }
                >
                  <ImageBackground
                    style={styles.image}
                    source={{
                      uri: `${item.images.jpg.image_url}`,
                    }}
                  >
                    <View style={styles.imageDetail}>
                      <Text numberOfLines={2} style={styles.textDetail}>
                        {title(item)}
                      </Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            )}
          />
        )}
        </View>
        
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e6e6e6",
  },
  flatView: {
    width: "100%",
  },
  flatRow: {
    flex: 1,
    justifyContent: "space-around",
  },
  itemView: {
    margin: 5,
    padding: 5,
    flexDirection: "column",
    flexWrap: "wrap",
  },
  imageWrapper: {
    height: 300,
    width: 200,
    overflow: "hidden",
    borderRadius: 5,
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    justifyContent: "flex-end",
  },
  imageDetail: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 5,
    height: 37,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.6)",
  },
  textDetail: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
});
export default Season;
